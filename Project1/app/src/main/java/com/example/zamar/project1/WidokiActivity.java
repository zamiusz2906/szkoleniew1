package com.example.zamar.project1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class WidokiActivity extends ActionBarActivity {

    private EditText mEditText;
    private TextView mTextView;
    private Button mButton;
    private static final String TAG = WidokiActivity.class.getName();
    private LinearLayout mLinear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState ==null){
            //Pierwsze otwarcie
        }else {
            //Obrot ekranu po zmianie jezyka
            // Po restarcie telefonu
        }


        setContentView(R.layout.activity_widoki);

        // FindViewbuId musi być koniecznie po setContentView
        mEditText = findView(R.id.pole_tekstowe);
       // mTextView = (TextView) findViewById(R.id.label);
        mButton = (Button) findViewById(R.id.przycisk);
        //widokiZKodu();
        mLinear = findView(R.id.tekst_kotener);
        mButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                klikPrzycisk(view);
            }
        });
    }
    public void klikPrzycisk(View mView){
        LinearLayout.LayoutParams mKontrolkaParrans = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);
        String text = mEditText.getText().toString();

        // Pokazuje z resprow wartosci
        String resString = getResources().getString(R.string.imie_hint);

        Log.d(TAG, text != null ? "Teskt ma długosć " + text.length() : "Null!");


        if((text == null || text.length()< 3)){
            //wyswietl ze za krótki tkst
            Toast.makeText(this, "Wpisałeś za krótki tekst", Toast.LENGTH_LONG).show();
            return;
        }
        mEditText.setText(""); // czyscicmy pole edycyjne
        // mTextView.setText(text);
        TextView mNowePole = new TextView(this);
        mNowePole.setLayoutParams(mKontrolkaParrans);
        mNowePole.setText(text);
        mLinear.addView(mNowePole);
    }
    public <T extends View> T findView(int indetyfikator){
        return (T)findViewById(indetyfikator);
    }
    public void widokiZKodu(){
        mLinear = new LinearLayout(this);
        mLinear.setGravity(Gravity.CENTER_HORIZONTAL);
        mLinear.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams mParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        mLinear.setLayoutParams(mParams);
        // Wspólne atrybuty szerokości i wysokosci dla pola tekstowego i edytowalnego
        LinearLayout.LayoutParams mKontrolkaParrans = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);




        mEditText = new EditText(this);
        mEditText.setLayoutParams(mKontrolkaParrans);
        mEditText.setSingleLine(true);
        mEditText.setHint("Podaj tekst");

        mTextView = new TextView(this);
        mTextView.setLayoutParams(mKontrolkaParrans);

        LinearLayout.LayoutParams mGuzikParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT , ViewGroup.LayoutParams.WRAP_CONTENT);

        mButton = new Button(this);
        mButton.setLayoutParams(mGuzikParams);
        mButton.setText("Gotowe!");
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                klikPrzycisk(view);
            }
        }
        );
    // Dodanie kontrolek do linearLayout
        mLinear.addView(mEditText);
        mLinear.addView(mTextView);
        mLinear.addView(mButton);



        // Wstawianie do kontenera obecnego w pliku XML
       //((RelativeLayout)findViewById(R.id.kontener)).addView(mLinear);
        setContentView(mLinear);

    }
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_widoki, menu);
        return true;
    }
    protected void wyczyscLinie(){
        List<View> mKolekcjaDoUsuniecia = new ArrayList<View>();
        for(int i = 0; i < mLinear.getChildCount(); i++){
            View mChild = mLinear.getChildAt(i);
            mKolekcjaDoUsuniecia.add(mChild);
        }

        for(View mChild: mKolekcjaDoUsuniecia){
            if(mChild.getClass().equals(TextView.class)){
                if(mChild instanceof TextView && !(mChild instanceof EditText) && !(mChild instanceof Button)){
                   mLinear.removeView(mChild);
                }
            }
        }
    }
    protected void wyczyscJednaLinie() {
        View mChild = mLinear.getChildAt(mLinear.getChildCount()-1);
        if(mChild instanceof TextView && !(mChild instanceof EditText) && !(mChild instanceof
        Button )){ mLinear.removeView(mChild);}
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.usun_wszystkie) {
            wyczyscLinie();
        }
        else if (id == R.id.usun_jedno){
            wyczyscJednaLinie();
        }



        return super.onOptionsItemSelected(item);
    }
}
